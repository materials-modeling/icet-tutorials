import os
import sys
import numpy as np
from icet import ClusterExpansion
from mchammer.ensembles import CanonicalEnsemble
from mchammer.calculators import ClusterExpansionCalculator
from mchammer.observers import BinaryShortRangeOrderObserver
from icet.tools.structure_generation import occupy_structure_randomly


# MC parameters
size = 4
T = 1500
n_steps = 10000

def generate_randomized_supercell(prim, size, c_Si):
    supercell = prim.repeat(size)
    N = len(supercell)
    n_Si = int(c_Si * N)
    symbols = ['Si'] * n_Si + ['Ge'] * (N - n_Si)
    np.random.shuffle(symbols) 
    supercell.set_chemical_symbols(symbols)
    return supercell


# parase parameters
c_Si = float(sys.argv[1])
temperatures = np.arange(2000, 700, -50)
size = 12

# setup atoms and calc
ce = ClusterExpansion.read('cluster_expansions/Si_Ge_ising_model.ce')
prim = ce._cluster_space.primitive_structure
atoms = prim.repeat(size)
n_atoms = len(atoms)

# mc parameters
n_steps = 5000 * n_atoms    # number of mc trial steps (mc_cycle*n_sites)
n_interval_data = 2 * n_atoms
n_interval_traj = 500 * n_atoms
dc_write_time = 60 * 5

# setup initial config with target concentration
c_target = dict(Si=c_Si, Ge=1 - c_Si)
occupy_structure_randomly(atoms, ce._cluster_space, c_target)
n_Si = atoms.get_chemical_symbols().count('Si')

for T in temperatures:
    # setup dc
    dc_fname = 'canonical_runs/dc_size{}_natoms-{}_nSi-{}_T{}.dc'.format(size, n_atoms, n_Si, T)
    os.makedirs(os.path.dirname(dc_fname), exist_ok=True)

    if os.path.isfile(dc_fname):
        print('data container already exists')

    # setup MC
    calc = ClusterExpansionCalculator(atoms, ce)
    sro = BinaryShortRangeOrderObserver(ce.get_cluster_space_copy(), atoms, interval=n_interval_data, radius=3.1)
    mc = CanonicalEnsemble(atoms, calc, temperature=T,
                           ensemble_data_write_interval=n_interval_data,
                           trajectory_write_interval=n_interval_traj,
                           data_container_write_period=dc_write_time,
                           dc_filename=dc_fname)
    mc.attach_observer(sro)

    # run MC
    print('Running {} natoms-{} nSi-{} T{}'.format(size, n_atoms, n_Si, T))
    mc.run(n_steps)
    atoms = mc.structure
