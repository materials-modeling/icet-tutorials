import os
import sys
import numpy as np
from icet import ClusterExpansion
from mchammer import DataContainer
from mchammer.calculators import ClusterExpansionCalculator
from mchammer.ensembles import SemiGrandCanonicalEnsemble


def generate_randomized_supercell(prim, size, c_Si):
    supercell = prim.repeat(size)
    N = len(supercell)
    n_Si = int(c_Si * N)
    symbols = ['Si'] * n_Si + ['Ge'] * (N-n_Si)
    np.random.shuffle(symbols) 
    supercell.set_chemical_symbols(symbols)
    return supercell


# parase parameters
T = float(sys.argv[1])
mu_vals = np.round(np.linspace(-0.02, 0.03, 41), 8)
#mu_vals = np.round(np.linspace(0.02, 0.06, 41), 8)
size = 12

# setup atoms and calc
ce = ClusterExpansion.read('cluster_expansions/Si_Ge_ising_model.ce')
prim = ce._cluster_space.primitive_structure
atoms = prim.repeat(size)
n_atoms = len(atoms)

# mc parameters
n_steps = 2000 * n_atoms    # number of mc trial steps (mc_cycle*n_sites)
n_interval_data = 2 * n_atoms
n_interval_traj = 500 * n_atoms
dc_write_time = 60 * 5


# setup initial config
atoms = generate_randomized_supercell(prim, size, 0.5)


for mu_Si in mu_vals:
    # setup dc
    dc_fname = 'sgc_runs/dc_size{}_T{}_muSi{}.dc'.format(size, T, mu_Si)
    os.makedirs(os.path.dirname(dc_fname), exist_ok=True)

    if os.path.isfile(dc_fname):
        print('data container already exists, skipping')
        dc = DataContainer.read(dc_fname)
        atoms = dc.get_trajectory()[-1]
        continue

    # setup MC
    chemical_potentials = dict(Si=mu_Si, Ge=0.0)
    calc = ClusterExpansionCalculator(atoms, ce)
    mc = SemiGrandCanonicalEnsemble(atoms, calc, T, chemical_potentials,
                                    ensemble_data_write_interval=n_interval_data,
                                    trajectory_write_interval=n_interval_traj,
                                    data_container_write_period=dc_write_time,
                                    data_container=dc_fname)

    # run MC
    print('Running {} mu_Si-{} T{}'.format(size, n_atoms, mu_Si, T))
    mc.run(n_steps)
    atoms = mc.structure
