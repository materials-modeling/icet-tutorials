[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/materials-modeling%2Ficet-tutorials/master)

# Tutorials
Clone this icet tutorial repository via

`git clone https://gitlab.com/materials-modeling/icet-tutorials`

You can launch an interactive version of this repository on
[Binder](https://mybinder.org/v2/gl/materials-modeling%2Ficet-tutorials/master)

# Learning objectives
In this tutorial you will
* Learn what Cluster Expansions (CE's) are and what they can be used for
* Get familiar with icet[1], a python package that allows the construction and sampling of alloy cluster expansions
* You will learn how to generate training sets for CE construction
* Learn to apply machine learning algorithms for training CE's
* Learn to sample a CE in various thermodynamic ensembles using Monte Carlo (MC) simulations
* Learn to efficiently analyze data from MC simulations

# Overview
This tutorial intend to give you an introduction to icet and cluster expansion construction.
The tutorial will consist of 4 parts, each part consists of a jupyter notebook along with necessary files.
Each part can be run separately.
The parts consist of

* stage 1: Structure generation, enumeration and how-to map relaxed configurations to the ideal lattice.
* stage 2: Cluster Expansion construction for the AgPd alloy using a provided ASE database calculated with DFT; The optimization algorithms we will investigate is ARDR, ordinary least-squares, LASSO and RFE
* stage 3: Sampling with a cluster expansion in the canonical and SGC ensemble for an Ising model
* stage 4: How-to analyze Monte Carlo sampling using a prepared densely sampled data containers generated using the Ising model.

During this tutorial developers of icet can also help you with specific help regarding your own project.


## Prerequisites
A computer, preferable with icet and jupyter notebook already installed


## Installation
`python3 -m pip install --user icet jupyter`

## Tutorials
There are four parts in this tutorial. Each tutorial comes with a jupyter notebook.

1. Structure generation, enumeration and how-to map relaxed configurations to the ideal lattice.
2. Cluster Expansion construction for the AgPd alloy using a provided ASE database calculated with DFT.
3.A Sampling with a cluster expansion in the canonical and SGC ensemble.
3.B How-to analyze Monte Carlo sampling using a prepared densely sampled data containers.



## References
[1] Ångqvist, M. , Muñoz, W. A., Rahm, J. M., Fransson, E. , Durniak, C. , Rozyczko, P. , Rod, T. H. and Erhart, P. (2019), ICET – A Python Library for Constructing and Sampling Alloy Cluster Expansions. Adv. Theory Simul., 2: 1900015. doi:10.1002/adts.201900015
