{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Welcome to the icet tutorial!\n",
    "In this first part of the tutorial we will construct a set of Ag-Pd alloy supercells. In the real world, one would then calculate the energy of these structures, usually with DFT. As DFT calculations are beyond the scope of this tutorial, we will here simply show how the structures can be generated, and then provide you with a database with calculated structures for part 2 of the tutorial.\n",
    "\n",
    "There are many ways to create structures to be used in a cluster expansion. One simple method is to create a fairly large supercell, and decorate it with Ag and Pd by random. Unfortunately, such a simple method will typically result in structures that are energetically very similar, as they would all, more or less, represent the random alloy. A more systematic approach is to create *all* possible structures having a certain size. This is usually referred to as \"enumeration\". In this tutorial we will show how to do this with icet.\n",
    "More information can be found [here](https://icet.materialsmodeling.org/advanced_topics/structure_enumeration.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Structure enumeration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first step is to create the primitive cell. Here, we create an Ag fcc cell with lattice parameter 4 Angstrom."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.build import bulk\n",
    "\n",
    "prim = bulk('Ag', a=4.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then we are ready to start the enumeration. Here, we enumerate all supercells having up to 8 atoms in the supercell. We save these in an ASE database.  For future plotting, we also save the concentration and size of the structures in a list. This should take no more than a few seconds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.db import connect\n",
    "from icet.tools import enumerate_structures\n",
    "\n",
    "db = connect('Ag-Pd-to-be-calculated.db')\n",
    "data = []\n",
    "for structure in enumerate_structures(prim, range(9), ['Ag', 'Pd']):\n",
    "    db.write(structure)\n",
    "    \n",
    "    c_Pd = structure.get_chemical_symbols().count('Pd') / len(structure)\n",
    "    data.append(dict(c_Pd=c_Pd, natoms=len(structure)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you now look in your database (for example with `ase db Ag-Pd-to-be-calculated.db` from the command line), you should find that your database includes 631 structures. We emphasize that these 631 structures are *all* existing symmetrically inequivalent supercells with 8 atoms or less.\n",
    "\n",
    "We now have a set of structures that we can calculate with DFT. For the next part of the tutorial, we will provide you with a database of already calculated structures."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get a better feel for the structures just generated we plot histograms over the concentrations in the structures as well as the number of atoms in the structures."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYUAAAEGCAYAAACKB4k+AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8/fFQqAAAACXBIWXMAAAsTAAALEwEAmpwYAAAUH0lEQVR4nO3de5RlZX3m8e8jyC2ioN1hCJcUCtEgRiUlEs04amccAgl0EkJwGQUl9ooxRkOGiOOaIWZW1uAyYxJnOZJWHNpZhIsEhk4wQUWQXARp7jfRDjebQWmNoiMaAv7mj7N5KdvqrtNVfc6uqvP9rFWr9n733uf8dt2eet99zrtTVUiSBPCUvguQJC0ehoIkqTEUJEmNoSBJagwFSVKzc98FLMSKFStqamqq7zIkaUm5/vrrv1ZVK2fbtqRDYWpqig0bNvRdhiQtKUnu29o2h48kSY2hIElqDAVJUmMoSJIaQ0GS1BgKkqTGUJAkNSMLhSQfTfJQkttmtL0vyReS3JLkkiR7zdj2riQbk9yV5D+Mqi5J0taNsqdwDnDUFm2fAg6rqp8Cvgi8CyDJocCJwPO7Y/5nkp1GWJskaRYje0dzVV2dZGqLtk/OWL0GOL5bPg44v6r+BbgnyUbgCOBzo6pPGoWp0y+btf3eM48ZcyXS/PR5TeFNwN90y/sBX56xbVPX9kOSrEmyIcmGzZs3j7hESZosvYRCkncDjwHnbu+xVbW2qqaranrlylnnc5IkzdPYJ8RLcjLwC8CqevIG0Q8AB8zYbf+uTZI0RmPtKSQ5Cvh94NiqemTGpvXAiUl2TXIQcAjw+XHWJkkaYU8hyXnAK4EVSTYBZzB4tdGuwKeSAFxTVb9ZVbcnuRC4g8Gw0lur6vFR1SZJmt0oX3302lmaz97G/n8E/NGo6pEkzc13NEuSGkNBktQYCpKkxlCQJDWGgiSpMRQkSY2hIElqDAVJUmMoSJIaQ0GS1BgKkqTGUJAkNYaCJKkxFCRJjaEgSWoMBUlSYyhIkhpDQZLUGAqSpMZQkCQ1hoIkqTEUJEmNoSBJagwFSVJjKEiSmpGFQpKPJnkoyW0z2p6Z5FNJvtR93rtrT5IPJNmY5JYkh4+qLknS1o2yp3AOcNQWbacDV1TVIcAV3TrAzwOHdB9rgA+NsC5J0laMLBSq6mrgn7doPg5Y1y2vA1bPaP9YDVwD7JVk31HVJkma3bivKexTVQ92y18B9umW9wO+PGO/TV2bJGmMervQXFUF1PYel2RNkg1JNmzevHkElUnS5Bp3KHz1iWGh7vNDXfsDwAEz9tu/a/shVbW2qqaranrlypUjLVaSJs24Q2E9cFK3fBJw6Yz2N3SvQjoSeHjGMJMkaUx2HtUDJzkPeCWwIskm4AzgTODCJKcA9wEndLt/Ajga2Ag8ArxxVHVJkrZuZKFQVa/dyqZVs+xbwFtHVYskaTi+o1mS1BgKkqTGUJAkNYaCJKkxFCRJjaEgSWoMBUlSYyhIkhpDQZLUGAqSpMZQkCQ1hoIkqTEUJEmNoSBJagwFSVJjKEiSGkNBktQYCpKkxlCQJDUju0ezNA5Tp182a/u9Zx4z5koWH782mg97CpKkxlCQJDWGgiSpMRQkSY2hIElqDAVJUtNLKCT53SS3J7ktyXlJdktyUJJrk2xMckGSXfqoTZIm2dhDIcl+wO8A01V1GLATcCLwXuBPqupg4BvAKeOuTZImXV/DRzsDuyfZGdgDeBB4NXBRt30dsLqf0iRpco09FKrqAeCPgfsZhMHDwPXAN6vqsW63TcB+sx2fZE2SDUk2bN68eRwlS9LE6GP4aG/gOOAg4MeAHwGOGvb4qlpbVdNVNb1y5coRVSlJk6mP4aOfA+6pqs1V9a/AxcDLgb264SSA/YEHeqhNkiZaH6FwP3Bkkj2SBFgF3AFcCRzf7XMScGkPtUnSROvjmsK1DC4o3wDc2tWwFngncGqSjcCzgLPHXZskTbpeps6uqjOAM7Zovhs4oodyJEkd39EsSWoMBUlSYyhIkhpDQZLUGAqSpMZQkCQ1hoIkqdnuUEiyd5KfGkUxkqR+DRUKSa5K8vQkz2TwTuQPJ3n/aEuTJI3bsD2FZ1TVt4BfBj5WVS9lMLGdJGkZGTYUdk6yL3AC8NcjrEeS1KNhQ+E9wOXAxqq6LsmzgS+NrixJUh+GnRDvwapqF5er6m6vKUjS8jNsT+F/DNkmSVrCttlTSPIzwMuAlUlOnbHp6cBOoyxMkjR+cw0f7QI8rdtvzxnt3+LJu6RJkpaJbYZCVX0W+GySc6rqvjHVJEnqybAXmndNshaYmnlMVb16FEVJkvoxbCh8HDgL+Ajw+OjKkST1adhQeKyqPjTSSiRJvRv2Jal/leS3kuyb5JlPfIy0MknS2A3bUzip+3zajLYCnr1jy5Ek9WmoUKiqg0ZdiCSpf0OFQpI3zNZeVR/bseVIkvo07PDRS2Ys7wasYnBfBUNBkpaRYYeP3jZzPclewPnzfdLu+I8AhzG4NvEm4C7gAgbvhbgXOKGqvjHf55Akbb/53qP5O8BCrjP8GfC3VfU84IXAncDpwBVVdQhwRbcuSRqjYa8p/BWD/+hhMBHeTwIXzucJkzwDeAVwMkBVPQo8muQ44JXdbuuAq4B3zuc5JEnzM+w1hT+esfwYcF9VbZrncx4EbAb+V5IXAtcDbwf2qaoHu32+Auwz28FJ1gBrAA488MB5liBJms1Qw0fdxHhfYDBT6t7Aowt4zp2Bw4EPVdWLGQxF/cBQUVUVT/ZMtqxlbVVNV9X0ypUrF1CGJGlLQ4VCkhOAzwO/yuA+zdcmme/U2ZuATVV1bbd+EYOQ+Gp3H2i6zw/N8/ElSfM07PDRu4GXVNVDAElWAp9m8Ad9u1TVV5J8Oclzq+ouBi9vvaP7OAk4s/t86fY+tiRpYYYNhac8EQidrzP/Vy4BvA04N8kuwN3AG7vHuzDJKcB9DHokkqQxGjYU/jbJ5cB53fqvAZ+Y75NW1U3A9CybVs33MSVJCzfXPZoPZvCqoNOS/DLws92mzwHnjro4SdJ4zdVT+FPgXQBVdTFwMUCSF3TbfnGEtUmSxmyu6wL7VNWtWzZ2bVMjqUiS1Ju5QmGvbWzbfQfWIUlaBOYKhQ1J3rxlY5LfYPBOZEnSMjLXNYV3AJckeR1PhsA0sAvwSyOsS5LUg22GQlV9FXhZklcxmOYa4LKq+szIK5Mkjd2w91O4ErhyxLVIknq2kHclS5KWGUNBktQYCpKkxlCQJDWGgiSpGXaWVE2YqdMvm7X93jOPGXMlWu78WVtc7ClIkhpDQZLUGAqSpMZQkCQ1hoIkqTEUJEmNoSBJagwFSVJjKEiSGkNBktQYCpKkprdQSLJTkhuT/HW3flCSa5NsTHJBkl36qk2SJlWfPYW3A3fOWH8v8CdVdTDwDeCUXqqSpAnWSygk2R84BvhItx7g1cBF3S7rgNV91CZJk6yvnsKfAr8PfL9bfxbwzap6rFvfBOw324FJ1iTZkGTD5s2bR16oJE2SsYdCkl8AHqqq6+dzfFWtrarpqppeuXLlDq5OkiZbHzfZeTlwbJKjgd2ApwN/BuyVZOeut7A/8EAPtUnSRBt7T6Gq3lVV+1fVFHAi8Jmqeh1wJXB8t9tJwKXjrk2SJt1iep/CO4FTk2xkcI3h7J7rkaSJ0+s9mqvqKuCqbvlu4Ig+65GkSbeYegqSpJ4ZCpKkxlCQJDWGgiSpMRQkSY2hIElqDAVJUmMoSJIaQ0GS1BgKkqTGUJAkNYaCJKkxFCRJjaEgSWoMBUlSYyhIkhpDQZLUGAqSpKbX23FK4zZ1+mWztt975jFjrmT7LeXatXTYU5AkNYaCJKkxFCRJjaEgSWq80KxFxYupi4/fk8liT0GS1Iw9FJIckOTKJHckuT3J27v2Zyb5VJIvdZ/3HndtkjTp+ugpPAb8XlUdChwJvDXJocDpwBVVdQhwRbcuSRqjsYdCVT1YVTd0y98G7gT2A44D1nW7rQNWj7s2SZp0vV5TSDIFvBi4Ftinqh7sNn0F2KevuiRpUvX26qMkTwP+EnhHVX0rSdtWVZWktnLcGmANwIEHHjiOUsfCV3hIi8Ok/y72EgpJnsogEM6tqou75q8m2beqHkyyL/DQbMdW1VpgLcD09PSswaHxm/RfJD3Jn4WlrY9XHwU4G7izqt4/Y9N64KRu+STg0nHXJkmTro+ewsuB1wO3Jrmpa/tPwJnAhUlOAe4DTuihNkmaaGMPhar6eyBb2bxqnLVIkn6Q72iWJDWGgiSpMRQkSY2hIElqDAVJUmMoSJIaQ0GS1BgKkqTGUJAkNYaCJKkxFCRJjaEgSWp6u8mOFsY566Xh+LuyfQwFLUtb+0OwHE3SuWr0HD6SJDWGgiSpMRQkSY3XFBY5x4ul0fB3a3b2FCRJjaEgSWocPpoQvlZby4U/y6NlT0GS1BgKkqTGUJAkNYaCJKkxFCRJzaILhSRHJbkrycYkp/ddjyRNkkX1ktQkOwEfBP49sAm4Lsn6qrpjRz+XL2uTtBSM+2/VYuspHAFsrKq7q+pR4HzguJ5rkqSJkarqu4YmyfHAUVX1G93664GXVtVvz9hnDbCmW30ucNc8n24F8LUFlLsUec6TwXOeDAs55x+vqpWzbVhUw0fDqKq1wNqFPk6SDVU1vQNKWjI858ngOU+GUZ3zYhs+egA4YMb6/l2bJGkMFlsoXAcckuSgJLsAJwLre65JkibGoho+qqrHkvw2cDmwE/DRqrp9RE+34CGoJchzngye82QYyTkvqgvNkqR+LbbhI0lSjwwFSVKz7ENhrmkzkuya5IJu+7VJpnooc4ca4pxPTXJHkluSXJHkx/uoc0cadnqUJL+SpJIs+ZcvDnPOSU7ovte3J/mLcde4ow3xs31gkiuT3Nj9fB/dR507SpKPJnkoyW1b2Z4kH+i+HrckOXzBT1pVy/aDwcXqfwKeDewC3AwcusU+vwWc1S2fCFzQd91jOOdXAXt0y2+ZhHPu9tsTuBq4Bpjuu+4xfJ8PAW4E9u7Wf7TvusdwzmuBt3TLhwL39l33As/5FcDhwG1b2X408DdAgCOBaxf6nMu9pzDMtBnHAeu65YuAVUkyxhp3tDnPuaqurKpHutVrGLwfZCkbdnqU/wq8F/jeOIsbkWHO+c3AB6vqGwBV9dCYa9zRhjnnAp7eLT8D+L9jrG+Hq6qrgX/exi7HAR+rgWuAvZLsu5DnXO6hsB/w5Rnrm7q2WfepqseAh4FnjaW60RjmnGc6hcF/GkvZnOfcdasPqKrZZxdbeob5Pv8E8BNJ/iHJNUmOGlt1ozHMOf8B8OtJNgGfAN42ntJ6s72/73NaVO9T0Hgl+XVgGvh3fdcySkmeArwfOLnnUsZtZwZDSK9k0Bu8OskLquqbfRY1Yq8Fzqmq/57kZ4D/neSwqvp+34UtFcu9pzDMtBltnyQ7M+hyfn0s1Y3GUFOFJPk54N3AsVX1L2OqbVTmOuc9gcOAq5Lcy2Dsdf0Sv9g8zPd5E7C+qv61qu4BvsggJJaqYc75FOBCgKr6HLAbg4njlqsdPjXQcg+FYabNWA+c1C0fD3ymuis4S9Sc55zkxcCfMwiEpT7ODHOcc1U9XFUrqmqqqqYYXEc5tqo29FPuDjHMz/b/YdBLIMkKBsNJd4+xxh1tmHO+H1gFkOQnGYTC5rFWOV7rgTd0r0I6Eni4qh5cyAMu6+Gj2sq0GUn+ENhQVeuBsxl0MTcyuKBzYn8VL9yQ5/w+4GnAx7tr6vdX1bG9Fb1AQ57zsjLkOV8OvCbJHcDjwGlVtWR7wUOe8+8BH07yuwwuOp+8lP/JS3Ieg2Bf0V0nOQN4KkBVncXgusnRwEbgEeCNC37OJfz1kiTtYMt9+EiStB0MBUlSYyhIkhpDQZLUGAqSpMZQ0JKT5PEkNyW5LcnHk+wxyz5/kOQ/9lHf9khycpIfm8dxq5McOmP9D7s3JEoLYihoKfpuVb2oqg4DHgV+s++CFuBkYNZQSLLTNo5bzWAWUACq6r9U1ad3aGWaSIaClrq/Aw4GSPLuJF9M8vfAc2fbOck+SS5JcnP38bKu/dSu53Fbknd0bVNJ7kzy4e5+BJ9Msnu37eAkn+4e44Ykz+naT0tyXTe3/Xu29ThJjmcw99S5Xc9n9yT3JnlvkhuAX03y5u7xbk7yl0n26Go+Fnhfd9xzkpzTPR5JVmVwP4FbM5iPf9eu/d4k7+nqvTXJ80b1TdHSZShoyermqvp54NYkP83g3egvYvAOz5ds5bAPAJ+tqhcymKf+9u7YNwIvZTAv0pu7qUBgMFfQB6vq+cA3gV/p2s/t2l8IvAx4MMlruv2P6Or46SSv2NrjVNVFwAbgdV3P57vdvl+vqsOr6nzg4qp6Sfc8dwKnVNU/Mpje4LTuuH+a8TXZDTgH+LWqegGDWQveMuP8v1ZVhwMfAhb98JrGz1DQUrR7kpsY/EG9n8FUJf8WuKSqHqmqb/HDc+I84dUM/iBSVY9X1cPAz3bHfqeq/h9wcfd4APdU1U3d8vXAVJI9gf2q6pLucb7X3Z/iNd3HjcANwPN4cgK6H3qcbZzfBTOWD0vyd0luBV4HPH8bx8Ggh3RPVX2xW1/H4EYtT7h4yBo0oZb13Edatr5bVS+a2ZDR3Rdp5gyyjwO7b2PfAP+tqv78BxoHt3jdnsf5zozlc4DVVXVzkpPpJrhbgCfqeBx//zULewpaLq4GVnfj8nsCv7iV/a6gG05JslOSZzC4LrG6G6//EeCXurZZVdW3gU1JVnePs2v3CqjLgTcleVrXvl+SH52j7m8zmNp7a/ZkMDT1VAY9hbmOu4tBb+bgbv31wGfnqEFqDAUtC1V1A4Nhl5sZ3Enuuq3s+nbgVd1wzPUM7vF7A4P/yD8PXAt8pKpunOMpXw/8TpJbgH8E/k1VfRL4C+Bz3eNfxLb/4NM971lPXGieZft/7mr6B+ALM9rPB07rLig/54nGqvoeg+sjH+9q+D5w1hw1SI2zpEqSGnsKkqTGUJAkNYaCJKkxFCRJjaEgSWoMBUlSYyhIkpr/D+53oq1Y9c1BAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYQAAAEGCAYAAABlxeIAAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8/fFQqAAAACXBIWXMAAAsTAAALEwEAmpwYAAAPsklEQVR4nO3de6xlZX3G8e8DI94Z1JkQBOKAh2qJaWMd76ZFpQ2GjlqLIrG1aSiUWqiXqhlbY/SfBuMl9GI0IyghJRBKtTKCYlVEYwzMgBcGkTrFQYaijBjHS1Jx5Nc/1jt2e5jLnmHts/aa+X6SnbP3u9d+9++smXOe8653r3elqpAk6ZChC5AkzQcDQZIEGAiSpMZAkCQBBoIkqVk2dAEPxYoVK2rVqlVDlyFJo3LTTTf9oKpWLm4fZSAkWQOsWVhYYOPGjUOXI0mjkuTOXbWP8pBRVa2vqrOXL18+dCmSdMAYZSBIkvpnIEiSAANBktQYCJIkwECQJDUGgiQJGGkgJFmTZN327duHLkWSDhijPDGtqtYD61evXn3W0LVI0r5Ytfbqh9zHlvNP7aGSBxvlCEGS1D8DQZIEGAiSpMZAkCQBBoIkqTEQJEmAgSBJagwESRJgIEiSmlEGgktXSFL/RhkIXkJTkvo3ykCQJPXPQJAkAQaCJKkxECRJgIEgSWoMBEkSYCBIkhoDQZIEGAiSpMZAkCQBBoIkqTEQJEmAgSBJakYZCC5/LUn9G2UguPy1JPVvlIEgSeqfgSBJAgwESVJjIEiSAANBktQYCJIkwECQJDUGgiQJMBAkSY2BIEkCDARJUmMgSJIAA0GS1BgIkiTAQJAkNQaCJAkwECRJjYEgSQIMBElSYyBIkgADQZLULBu6gJ2SvBw4FTgcuKiqPjNsRZJ0cJnpCCHJR5Lcm2TTovZTktyeZHOStQBV9R9VdRZwDnD6LOuSJD3YrEcIFwP/AlyysyHJocAHgN8HtgIbklxVVd9sm7y9PS9JD9mqtVf30s+W80/tpZ95NtMRQlV9EfjhouZnAZur6o6quh+4HHhZOu8GPlVVN++uzyRnJ9mYZOO2bdtmV7wkHWSGmFQ+Grhr4vHW1nYecDJwWpJzdvfiqlpXVauravXKlStnW6kkHUTmZlK5qv4J+Keh65Ckg9UQI4S7gWMnHh/T2iRJAxoiEDYAJyQ5LslhwKuBq/algyRrkqzbvn37TAqUpIPRrD92ehnwFeApSbYmObOqdgDnAtcCtwFXVNWt+9JvVa2vqrOXL1/ef9GSdJCa6RxCVZ2xm/ZrgGtm+d6SpH3j0hWSJMBAkCQ1c/Ox032RZA2wZmFhYehSJC3imcHjNcoRgpPKktS/UQaCJKl/BoIkCTAQJEnNKAPBM5UlqX+jDAQnlSWpf6MMBElS/wwESRJgIEiSmlEGgpPKktS/UQaCk8qS1L9RBoIkqX8GgiQJMBAkSY2BIEkCDARJUjPKQPBjp5LUv1EGgh87laT+jTIQJEn9MxAkSYCBIElqDARJEmAgSJKafQ6EJI9L8luzKEaSNJypAiHJF5IcnuTxwM3Ah5O8f7alSZKW0rQjhOVV9WPgFcAlVfVs4OTZlbVnnpgmSf2bNhCWJTkKeBXwyRnWMxVPTJOk/k0bCO8CrgU2V9WGJMcD355dWZKkpbZsyu3uqapfTSRX1R3OIUjSgWXaEcI/T9kmSRqpPY4QkjwXeB6wMsmbJp46HDh0loVJkpbW3g4ZHQY8pm332In2HwOnzaooSdLS22MgVNX1wPVJLq6qO5eoJknSAKadVH54knXAqsnXVNWLZlGUJGnpTRsI/wZ8CLgQ+OXsypEkDWXaQNhRVR+caSX7IMkaYM3CwsLQpUjSAWPaj52uT/K6JEclefzO20wr2wPPVJak/k07Qviz9vUtE20FHN9vOZKkoUwVCFV13KwLkbR/Vq29upd+tpx/ai/9aLymCoQkr91Ve1Vd0m85kqShTHvI6JkT9x8BvJjuuggGgiQdIKY9ZHTe5OMkRwCXz6IgSdIw9veayj8DnFeQpAPItHMI6+k+VQTdona/CVwxq6IkSUtv2jmE907c3wHcWVVbZ1CPJGkgUx0yaovcfYtuxdPHAffPsihJ0tKbKhCSvAq4EXgl3XWVb0ji8teSdACZ9pDR3wPPrKp7AZKsBD4LXDmrwiRJS2vaTxkdsjMMmvv24bWSpBGYdoTw6STXApe1x6cD18ymJEnSEPZ2TeUF4MiqekuSVwAvaE99Bbh01sVJkpbO3g77XEB3/WSq6mNV9aaqehPw8fbcIJKsSbJu+/btQ5UgSQecvQXCkVV1y+LG1rZqJhVNweshSFL/9jaHcMQenntkj3VIo+Fy0zpQ7W2EsDHJWYsbk/wFcNNsSpIkDWFvI4Q3AB9P8hr+PwBWA4cBfzTDuiRJS2yPgVBV3weel+SFwNNa89VV9fmZVyZJWlLTXg/hOuC6GdciSRqQZxtLkgADQZLUGAiSJMBAkCQ1BoIkCTAQJEmNgSBJAgwESVJjIEiSAANBktQYCJIkwECQJDUGgiQJMBAkSY2BIEkC5igQkhyf5KIkVw5diyQdjGYaCEk+kuTeJJsWtZ+S5PYkm5OsBaiqO6rqzFnWI0navVmPEC4GTplsSHIo8AHgJcCJwBlJTpxxHZKkvZhpIFTVF4EfLmp+FrC5jQjuBy4HXjbLOiRJezfEHMLRwF0Tj7cCRyd5QpIPAU9P8rbdvTjJ2Uk2Jtm4bdu2WdcqSQeNZUMXsFNV3QecM8V264B1AKtXr65Z1yVJB4shRgh3A8dOPD6mtUmSBjREIGwATkhyXJLDgFcDVw1QhyRpwqw/dnoZ8BXgKUm2JjmzqnYA5wLXArcBV1TVrfvY75ok67Zv395/0ZJ0kJrpHEJVnbGb9muAax5Cv+uB9atXrz5rf/uQJP26uTlTWZI0LANBkgSMNBCcQ5Ck/o0yEKpqfVWdvXz58qFLkaQDxigDQZLUPwNBkgQYCJKkZpSB4KSyJPVvlIHgpLIk9W+UgSBJ6p+BIEkCDARJUmMgSJKAObpi2r5IsgZYs7CwMHQpWgKr1l7dSz9bzj+1l36kA9UoRwh+ykiS+jfKQJAk9c9AkCQBBoIkqTEQJEmAgSBJakYZCC5uJ0n9G2Ug+LFTSerfKANBktQ/A0GSBBgIkqTGQJAkAQaCJKkxECRJwEgDwfMQJKl/owwEz0OQpP6NMhAkSf0zECRJgIEgSWoMBEkSYCBIkhoDQZIEGAiSpMZAkCQBBoIkqRllILh0hST1b5SB4NIVktS/UQaCJKl/BoIkCTAQJEmNgSBJAgwESVJjIEiSAANBktQYCJIkwECQJDUGgiQJMBAkSY2BIEkCDARJUrNs6AL2R5I1wJqFhYWhS+nNqrVX99LPlvNP7aWfeatH0uyNcoTg8teS1L9RBoIkqX8GgiQJMBAkSY2BIEkCDARJUmMgSJIAA0GS1BgIkiQAUlVD17DfkmwD7pzhW6wAfjDD/sfO/bN77ps9c//s3lLsmydV1crFjaMOhFlLsrGqVg9dx7xy/+ye+2bP3D+7N+S+8ZCRJAkwECRJjYGwZ+uGLmDOuX92z32zZ+6f3Rts3ziHIEkCHCFIkhoDQZIEGAi7lOSUJLcn2Zxk7dD1zJMkxya5Lsk3k9ya5PVD1zSPkhya5KtJPjl0LfMkyRFJrkzyrSS3JXnu0DXNkyRvbD9Xm5JcluQRS/n+BsIiSQ4FPgC8BDgROCPJicNWNVd2AH9bVScCzwH+2v2zS68Hbhu6iDn0j8Cnq+qpwG/jPvqVJEcDfwOsrqqnAYcCr17KGgyEB3sWsLmq7qiq+4HLgZcNXNPcqKp7qurmdv8ndD/QRw9b1XxJcgxwKnDh0LXMkyTLgd8FLgKoqvur6keDFjV/lgGPTLIMeBTwP0v55gbCgx0N3DXxeCv+wtulJKuApwM3DFzKvLkAeCvwwMB1zJvjgG3AR9vhtAuTPHroouZFVd0NvBf4LnAPsL2qPrOUNRgI2i9JHgP8O/CGqvrx0PXMiyR/CNxbVTcNXcscWgb8DvDBqno68DPAObomyePojkYcBzwReHSSP1nKGgyEB7sbOHbi8TGtTU2Sh9GFwaVV9bGh65kzzwdemmQL3eHGFyX512FLmhtbga1VtXNEeSVdQKhzMvCdqtpWVb8APgY8bykLMBAebANwQpLjkhxGN6lz1cA1zY0koTsGfFtVvX/oeuZNVb2tqo6pqlV0/3c+X1VL+lfevKqq7wF3JXlKa3ox8M0BS5o33wWek+RR7efsxSzxpPuypXyzMaiqHUnOBa6lm+X/SFXdOnBZ8+T5wJ8CtyT5Wmv7u6q6ZriSNCLnAZe2P7buAP584HrmRlXdkORK4Ga6T/N9lSVexsKlKyRJgIeMJEmNgSBJAgwESVJjIEiSAANBktQYCBqVJJXkfROP35zknT31fXGS0/roay/v88q20ud1U25/RJLXzbouyUDQ2PwceEWSFUMXMqktRjatM4GzquqFU25/BGAgaOYMBI3NDrqTdd64+InFf+En+Wn7elKS65N8IskdSc5P8pokNya5JcmTJ7o5OcnGJP/V1iXaeW2D9yTZkOQbSf5yot8vJbmKXZxxm+SM1v+mJO9ube8AXgBclOQ9i7Z/TJLPJbm5vW7nKrvnA09O8rVWR9rXTW270/fl+2wjlE1Jvp7ki/v3z6ADUlV58zaaG/BT4HBgC7AceDPwzvbcxcBpk9u2rycBPwKOAh5OtzbVu9pzrwcumHj9p+n+UDqBbu2dRwBnA29v2zwc2Ei3ANlJdAu0HbeLOp9ItxTBSroVAT4PvLw99wW6Ne8Xv2YZcHi7vwLYDARYBWya2O6Pgf+kO5P+yPY+R+3D93kLcHS7f8TQ/6be5ufmCEGjU93qqpfQXUxkWhuqu5bDz4H/BnYuK3wL3S/cna6oqgeq6tt0Sys8FfgD4LVtqY4bgCfQBQbAjVX1nV283zOBL1S3UNkO4FK6awHsSYB/SPIN4LN0y64fuYvtXgBcVlW/rKrvA9e395v2+/wycHGSs+hCRQJcy0jjdQHdmi8fnWjbQTsMmuQQ4LCJ534+cf+BiccP8Os/B4vXcim6X9TnVdW1k08kOYluhNCX19CNKJ5RVb9oK6bu6yUU9/p9VtU5SZ5NdxGfm5I8o6rue0iV64DgCEGjVFU/BK6gm6DdaQvwjHb/pcDD9qPrVyY5pB1vPx64nW6hw79qy36T5DemuLDLjcDvJVnRLst6Bt1f8nuynO5aCr9I8kLgSa39J8BjJ7b7EnB6m9tYSTfyuHHabzDJk6vqhqp6B90Fa47d22t0cHCEoDF7H3DuxOMPA59I8nW6uYD9+ev9u3S/XA8Hzqmq/01yId3hlpvbssTbgJfvqZOquifJWuA6uhHG1VX1ib2896XA+iS30M1TfKv1dV+SLyfZBHyK7mpszwW+TjeCeWtVfS/JU6f8Ht+T5IRW1+daP5KrnUqSOh4ykiQBBoIkqTEQJEmAgSBJagwESRJgIEiSGgNBkgTA/wHsqdZdZSWxlQAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "df = pd.DataFrame(data)\n",
    "\n",
    "plt.figure()\n",
    "plt.hist(df.c_Pd, bins=51)\n",
    "plt.xlabel('Pd concentration')\n",
    "plt.ylabel('Counts')\n",
    "\n",
    "plt.figure()\n",
    "plt.bar(range(9), np.bincount(df.natoms), width=0.5, align='center', log=True)\n",
    "plt.xlabel('Number of atoms')\n",
    "plt.ylabel('Counts');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Two things are worth noting: \n",
    "\n",
    "1. the number of structures grows very rapidly with supercell size, \n",
    "\n",
    "2. the number of structures as a function of concentration is sharply peaked around $c=0.5$ (it gets sharper the larger we go). This is something you may want to consider when constructing a training set so that your training data is not biased towards some concentration, but that is beyond the scope of this tutorial."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
